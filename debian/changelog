misc3d (0.9-1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Oct 2021 16:20:16 -0500

misc3d (0.9-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 10)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Sep 2020 09:16:28 -0500

misc3d (0.8-4-5) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 15 May 2020 21:13:10 -0500

misc3d (0.8-4-4) unstable; urgency=medium

  * Source-only upload
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 16 Aug 2019 17:41:18 -0500

misc3d (0.8-4-3) unstable; urgency=medium

  * Rebuilding for R 3.5.0 transition
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Depends: on ${misc:Depends}
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/compat: Increase level to 9
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem
  * debian/README.source: Added

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 02 Jun 2018 09:54:13 -0500

misc3d (0.8-4-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for r-api-3.4 transition

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 29 Sep 2017 18:52:10 +0200

misc3d (0.8-4-2) unstable; urgency=medium

  * debian/compat: Created			(Closes: #829373)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 07 Jul 2016 05:45:03 -0500

misc3d (0.8-4-1) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Apr 2013 21:08:00 -0500

misc3d (0.8-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 08 Aug 2012 17:11:13 -0500

misc3d (0.8-2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 06 Feb 2012 00:31:37 -0600

misc3d (0.8-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 20 May 2011 14:18:29 -0500

misc3d (0.8-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 May 2011 17:09:19 -0500

misc3d (0.7-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 24 Sep 2010 13:28:21 -0500

misc3d (0.7-0-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 02 Nov 2009 19:41:10 -0600

misc3d (0.7-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 18 Jul 2009 16:37:25 -0500

misc3d (0.6-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.2

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 17 Jun 2009 19:24:30 -0500

misc3d (0.6-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 28 May 2008 13:46:17 -0500

misc3d (0.5-2-1) unstable; urgency=low

  * New upstream release
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 06 May 2008 20:56:21 -0500

misc3d (0.4-1-1) unstable; urgency=low

  * New upstream release
  
  * Built with R 2.6.0, so setting (Build-)Depends: to 
    'r-base-(core|dev) >= 2.6.0' to prevent move to testing before R 2.6.0

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 22 Oct 2007 20:10:43 -0500

misc3d (0.4-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Standards-Version: increased to 3.7.2
  
  * debian/control: Changed Architecture: to all

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  6 Jun 2006 17:25:13 -0500

misc3d (0.3-1-5) unstable; urgency=low

  * debian/control: Removed unnecessary Build-Depends: on libgl-mesa-dev 
    and libglu-mesa-dev as GL is provided directly via the r-cran-rgl
    package					       	(Closes: #365237)
  
  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.0)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 28 Apr 2006 19:53:13 -0500

misc3d (0.3-1-4) unstable; urgency=low

  * debian/post{inst,rm}: No longer call R to update html help index
  * debian/watch: Updated to Version: 3

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 10 Apr 2006 19:49:17 -0500

misc3d (0.3-1-3) unstable; urgency=low

  * DESCRIPTION: Set LazyLoad: no to avoid pulling in r-cran-rgl which
    requires an X11 server 				(Closes: #361871)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 10 Apr 2006 19:35:14 -0500

misc3d (0.3-1-2) unstable; urgency=low

  * debian/control: Removed xlibs-dev from Build-Depends: 
  * debian/control: Upgraded Standards-Version: to 3.6.2

 -- Dirk Eddelbuettel <edd@debian.org>  Sun,  8 Jan 2006 19:52:11 -0600

misc3d (0.3-1-1) unstable; urgency=low

  * Initial Debian Release 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  4 Jun 2005 11:33:36 -0500


